package test;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.EmailRule;
import domain.Person;

public class EmailRuleTest {

	EmailRule rule = new EmailRule();
	
	@Test
	public void checker_if_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_if_empty(){
		Person p = new Person();
		p.setEmail("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_email(){
		Person p = new Person();
		p.setEmail("brakmaila@nic.com");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	}