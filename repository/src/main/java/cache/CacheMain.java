package cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import domain.Person;

public class CacheMain<T> implements ICache<T> {

	private Class<T> EntityClass;
	private Map<Integer, T> CacheItems;
	private boolean IFvalid;

	public CacheMain(Session session, Class<T> entityClass, ICache<Person> cache) {
		this.CacheItems = new HashMap<Integer, T>();
		this.EntityClass = entityClass;
		this.IFvalid = false;
	}

	public List<T> getAll() {
		List<T> result = new ArrayList<T>();
		result.addAll(CacheItems.values());
		return result;
	}

	public T getById(int id) {
		return CacheItems.get(id);
	}

	public boolean isValid() {
		return IFvalid;
	}

	public void set(Map<Integer, T> list) {
		this.CacheItems.putAll(list);
		this.IFvalid = true;
	}

	public void invalidate() {
		this.IFvalid = false;
	}
}