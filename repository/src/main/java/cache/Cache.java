package cache;

import domain.Address;
import domain.EnumValue;
import domain.Person;
import domain.PhoneNumber;
import domain.User;

public class Cache {

	private static Cache instance;

	public static Cache getInstance() {
		if (instance == null) {
			instance = new Cache();
		}
		return instance;
	}
	
	private CacheMain<Person> PersonCache;
	private CacheMain<User> UserCache;
	private CacheMain<Address> AddressCache;
	private CacheMain<PhoneNumber> PhoneCache;
	private CacheMain<EnumValue> EnumValueCache;

	private Cache() {
		PersonCache = new CacheMain<Person>(null, Person.class, PersonCache);
		UserCache = new CacheMain<User>(null, User.class, PersonCache);
		AddressCache = new CacheMain<Address>(null, Address.class, PersonCache);
		PhoneCache = new CacheMain<PhoneNumber>(null, PhoneNumber.class, PersonCache);
		EnumValueCache = new CacheMain<EnumValue>(null, EnumValue.class, PersonCache);
	}

	public CacheMain<Person> getPersoncache() {
		return PersonCache;
	}

	public CacheMain<User> getUser() {
		return UserCache;
	}

	public CacheMain<Address> getAddress() {
		return AddressCache;
	}

	public CacheMain<PhoneNumber> getPhoneNumber() {
		return PhoneCache;
	}

	public CacheMain<EnumValue> getEnumValue() {
		return EnumValueCache;
	}

}