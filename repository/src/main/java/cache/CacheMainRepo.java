package cache;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import repositories.IRepository;

public abstract class CacheMainRepo<T> implements IRepository<T> {

	private Session session;
	private Class<T> entityClass;
	private ICache<T> cache;
	
	protected CacheMainRepo(Session session, Class<T> entityClass, ICache<T> cache){
		this.session = session;
		this.entityClass = entityClass;
		this.cache = cache;
	}

	public List<T> getAll() {
		if(!cache.isValid()) {
			updateCache();
		}
		return cache.getAll();
	}

	public T getById(int id) {
		if(!cache.isValid()) {
			updateCache();
		}	
		return cache.getById(id);
	}
	
	private void updateCache() {
		List<T> entity = createCriteria().list();
		cache.set(getMap(entity));
	}
	
	protected abstract Map<Integer, T> getMap(List<T> entities);

	private Criteria createCriteria() {
		return session.createCriteria(entityClass);
	}

	public void save(T entity) {
		session.saveOrUpdate(entity);
		cache.invalidate();
	}
	
	public void delete(int id) {
		T entityToDelete = getById(id);
		session.delete(entityToDelete);
		cache.invalidate();
	}
}