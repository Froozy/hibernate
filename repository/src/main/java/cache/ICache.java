package cache;

import java.util.List;
import java.util.Map;

public interface ICache<T> {

	public List<T> getAll();
	public T getById(int id);
	public boolean isValid();
	public void invalidate();
	public void set(Map<Integer,T> list);
}