package domain;

public class RolePermission extends Entity {

	private int roleId;
	private int permId;

	public RolePermission(int id, EntityState state, int permID, int roleID) {
		super(id, state);
		this.permId = permID;
		this.roleId = roleID;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getPermissionId() {
		return permId;
	}

	public void setPermissionId(int permId) {
		this.permId = permId;
	}
}