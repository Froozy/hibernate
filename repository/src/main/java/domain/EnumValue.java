package domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="EnumValue")
public class EnumValue extends domain.Entity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int intKey;
	
	@Column(name="STRINGKEY") private String stringkey;
	@Column(name="VALUE") private int value;
	@Column(name="ENUMNAME") private int enumName;

	public int getIntKey() {
		return intKey;
	}

	public void setIntKey(int intKey) {
		this.intKey = intKey;
	}

	public String getStringkey() {
		return stringkey;
	}

	public void setStringkey(String stringkey) {
		this.stringkey = stringkey;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getEnumerationName() {
		return enumName;
	}

	public void setEnumName(int enumName) {
		this.enumName = enumName;
	}
}