package domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Entity;

@Entity
@Table(name="USER")
public class User extends domain.Entity implements Serializable {

	private static final long serialVersionUID = 7526472295622776147L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="LOGIN") private String login;
	@Column(name="PASSWORD") private String password;

	@Transient
	private List<UserRole> roles;
	@Transient
	private List<RolePermission> permissions;
	
	@OneToOne(mappedBy="USER")
	private Person person;

	public User() {
		permissions = new ArrayList<RolePermission>();
		roles = new ArrayList<UserRole>();
		
	}

	public List<RolePermission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<RolePermission> permissions) {
		this.permissions = permissions;
	}

	public List<UserRole> getRoles() {
		return roles;
	}

	public void setRoles(List<UserRole> roles) {
		this.roles = roles;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
		if (!this.equals(person.getUser()))
			person.setUser(this);
	}
}