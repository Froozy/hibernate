package domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@javax.persistence.Entity
public abstract class Entity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	EntityState state;

	public Entity() {
	}

	public Entity(int id, EntityState state) {
		this.id = id;
		this.state = state;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EntityState getState() {
		return state;
	}

	public void setState(EntityState state) {
		this.state = state;
	}
}