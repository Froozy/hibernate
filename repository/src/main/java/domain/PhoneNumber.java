package domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PHONENUMBER")
public class PhoneNumber extends domain.Entity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="COUNTRYPREFIX") private String countryPrefix;
	@Column(name="CITYPREFIX") private String cityPrefix;
	@Column(name="NUMBER") private String number;
	@Column(name="TYPEID") private String typeId;

	public String getCountryPrefix() {
		return countryPrefix;
	}

	public void setCountryPrefix(String countryPrefix) {
		this.countryPrefix = countryPrefix;
	}

	public String getCityPrefix() {
		return cityPrefix;
	}

	public void setCityPrefix(String cityPrefix) {
		this.cityPrefix = cityPrefix;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

}