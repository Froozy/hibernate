package domain;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="PERSON")
public class Person extends domain.Entity implements Serializable {

	private static final long serialVersionUID = 7526472295622776147L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="FIRSTNAME") private String firstName;
	@Column(name="SURNAME") private String surname;
	@Column(name="PESEL") private String pesel;
	@Column(name="NIP") private String nip;
	@Column(name="DATE") private Date date;
	@Column(name="EMAIL") private String email;
	
	@OneToOne
	@JoinColumn(name="PERSON")
	private User user;

	@OneToMany(mappedBy="PERSON")
	private List<PhoneNumber> phoneNumber;
	
	@OneToMany(mappedBy="PERSON")
	private List<Address> address;

	public Person() {
		phoneNumber = new ArrayList<PhoneNumber>();
		address = new ArrayList<Address>();
	}

	public List<PhoneNumber> getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(List<PhoneNumber> phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<Address> getAddress() {
		return address;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setAdsress(List<Address> adsress) {
		this.address = adsress;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		if (!this.equals(user.getPerson()))
			user.setPerson(this);
		this.user = user;
	}
}