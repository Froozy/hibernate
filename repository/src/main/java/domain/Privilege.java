package domain;

import java.util.ArrayList;
import java.util.List;

public class Privilege extends Entity {

	private String name;
	
	private List<Role> rolesList;

	public Privilege()
	{
		rolesList = new ArrayList<Role>();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Role> getRole() {
		return rolesList;
	}

	public void setRoles(List<Role> roles) {
		this.rolesList = roles;
	}
	
	
}