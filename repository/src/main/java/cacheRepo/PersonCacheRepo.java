package cacheRepo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import cache.CacheMainRepo;
import cache.ICache;
import domain.Person;

public class PersonCacheRepo extends CacheMainRepo<Person> {

	public PersonCacheRepo(Session session, ICache<Person> cache) {
		super(session, Person.class, cache);
	}

	protected Map<Integer, Person> getMap(List<Person> entities) {
		Map<Integer, Person> map = new HashMap<Integer, Person>();
		for (Person person : entities) {
			map.put(person.getId(), person);
		}
		return map;
	}

	public void update(Person entity) {
		// TODO Auto-generated method stub
		
	}

	public void delete(Person entity) {
		// TODO Auto-generated method stub
		
	}

	public Person get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}