package cacheRepo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import cache.CacheMainRepo;
import cache.ICache;
import domain.PhoneNumber;

public class PhoneCacheRepo extends CacheMainRepo<PhoneNumber> {

	public PhoneCacheRepo(Session session, ICache<PhoneNumber> cache) {
		super(session, PhoneNumber.class, cache);
	}

	protected Map<Integer, PhoneNumber> getMap(List<PhoneNumber> entities) {
		Map<Integer, PhoneNumber> map = new HashMap<Integer, PhoneNumber>();
		for (PhoneNumber number : entities) {
			map.put(number.getId(), number);
		}
		return map;
	}

	public void update(PhoneNumber entity) {
		// TODO Auto-generated method stub
		
	}

	public void delete(PhoneNumber entity) {
		// TODO Auto-generated method stub
		
	}

	public PhoneNumber get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}