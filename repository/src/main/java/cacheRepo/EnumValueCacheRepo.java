package cacheRepo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import cache.CacheMainRepo;
import cache.ICache;
import domain.EnumValue;

public class EnumValueCacheRepo extends CacheMainRepo<EnumValue> {

	public EnumValueCacheRepo(Session session, ICache<EnumValue> cache) {
		super(session, EnumValue.class, cache);
	}

	@Override
	protected Map<Integer, EnumValue> getMap(List<EnumValue> entities) {
		Map<Integer, EnumValue> map = new HashMap<Integer, EnumValue>();
		for (EnumValue value : entities) {
			map.put(value.getId(), value);
		}
		return map;
	}

	public void update(EnumValue entity) {
		// TODO Auto-generated method stub
		
	}

	public void delete(EnumValue entity) {
		// TODO Auto-generated method stub
		
	}

	public EnumValue get(int id) {
		// TODO Auto-generated method stub
		return null;
	}
}