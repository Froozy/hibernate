package cacheRepo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import cache.CacheMainRepo;
import cache.ICache;
import domain.User;

public class UserCacheRepo extends CacheMainRepo<User> {

	public UserCacheRepo(Session session, ICache<User> cache) {
		super(session, User.class, cache);
	}

	@Override
	protected Map<Integer, User> getMap(List<User> entities) {
		Map<Integer, User> map = new HashMap<Integer, User>();
		for (User user : entities) {
			map.put(user.getId(), user);
		}
		return map;
	}

	public void update(User entity) {
		// TODO Auto-generated method stub
		
	}

	public void delete(User entity) {
		// TODO Auto-generated method stub
		
	}

	public User get(int id) {
		// TODO Auto-generated method stub
		return null;
	}
}
