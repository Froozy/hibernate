package cacheRepo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import cache.CacheMainRepo;
import cache.ICache;
import domain.Address;

public class AddressCacheRepo extends CacheMainRepo<Address> {

	public AddressCacheRepo(Session session, ICache<Address> cache) {
		super(session, Address.class, cache);
	}

	protected Map<Integer, Address> getMap(List<Address> entities) {
		Map<Integer, Address> map = new HashMap<Integer, Address>();
		for (Address address : entities) {
			map.put(address.getId(), address);
		}
		return map;
	}

	public void update(Address entity) {
		// TODO Auto-generated method stub
		
	}

	public void delete(Address entity) {
		// TODO Auto-generated method stub
		
	}

	public Address get(int id) {
		// TODO Auto-generated method stub
		return null;
	}
}