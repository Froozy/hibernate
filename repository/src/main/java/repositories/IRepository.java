package repositories;

import java.util.List;

public interface IRepository<T> {

	public List<T> getAll();
	public void save(T entity);
	public void update(T entity);
	public void delete(T entity);
	public T get(int id);
}