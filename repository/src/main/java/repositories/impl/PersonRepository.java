package repositories.impl;

import org.hibernate.Session;

import repositories.BaseRepo;
import repositories.IRepository;
import domain.Person;

public class PersonRepository extends BaseRepo<Person> implements IRepository<Person> {

	public PersonRepository(Session s) {
		super(s, Person.class);
	}	
}
