package repositories.impl;

import org.hibernate.Session;

import repositories.BaseRepo;
import repositories.IRepository;
import domain.PhoneNumber;

public class PhoneRepo extends BaseRepo<PhoneNumber> implements IRepository<PhoneNumber> {

	public PhoneRepo(Session s) {
		super(s, PhoneNumber.class);
	}	
}