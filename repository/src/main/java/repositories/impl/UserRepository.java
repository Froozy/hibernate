package repositories.impl;

import org.hibernate.Session;

import repositories.BaseRepo;
import repositories.IRepository;
import domain.User;

public class UserRepository extends BaseRepo<User> implements IRepository<User> {

	public UserRepository(Session s) {
		super(s, User.class);
	}
}