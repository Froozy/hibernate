package repositories;

import org.hibernate.Session;

import domain.Address;

public class AddressRepository extends BaseRepo<Address> implements IRepository<Address> {

	public AddressRepository(Session s) {
		super(s, Address.class);
	}
}