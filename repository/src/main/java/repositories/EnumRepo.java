package repositories;

import org.hibernate.Session;

import domain.EnumValue;

public class EnumRepo extends BaseRepo<EnumValue> implements IRepository<EnumValue> {

	public EnumRepo(Session session, Class<EnumValue> entityClass) {
		super(session, entityClass);
		// TODO Auto-generated constructor stub
	}

	public EnumRepo(Session s) {
		super(s, EnumValue.class);
	}	
}