package repositories;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import unitofwork.UnitOfWork;

import com.mysql.jdbc.Connection;

public class BaseRepo<T> implements IRepository<T>{

	private Session session;
	private Class<T> entityClass;
	
	public BaseRepo(Session session, Class<T> entityClass){
		this.session = session;
		this.entityClass = entityClass;
	}

	public List<T> getAll() {
		List<T> entity = createCriteria().list();
		return entity;
	}
	
	public T getById(int id) {
		Criteria criteria = createCriteria();
		criteria.add(Restrictions.eq("id", id));
		return (T) criteria.list().get(0);
		
	}
	private Criteria createCriteria(){
		return session.createCriteria(entityClass);
	}

	public void save(T entity) {
		session.saveOrUpdate(entity);		
	}

	public void delete(int id) {
		T entityToDelete = getById(id);
		session.delete(entityToDelete);		
	}

	public void update(T entity) {
		// TODO Auto-generated method stub
		
	}

	public void delete(T entity) {
		// TODO Auto-generated method stub
		
	}

	public T get(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}